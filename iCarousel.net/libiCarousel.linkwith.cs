using System;
using MonoTouch.ObjCRuntime;

[assembly: LinkWith ("libiCarousel.a", LinkTarget.ArmV6 | LinkTarget.ArmV7 | LinkTarget.Simulator, 
                     ForceLoad = true, Frameworks="CoreGraphics QuartzCore UIKit")]
