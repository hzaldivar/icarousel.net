using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;
using MonoTouch.UIKit;
using MonoTouch.CoreGraphics;
using System.Collections.Generic;
using MonoTouch.CoreAnimation;

namespace iCarousel.net
{
	// The first step to creating a binding is to add your native library ("libNativeLibrary.a")
	// to the project by right-clicking (or Control-clicking) the folder containing this source
	// file and clicking "Add files..." and then simply select the native library (or libraries)
	// that you want to bind.
	//
	// When you do that, you'll notice that MonoDevelop generates a code-behind file for each
	// native library which will contain a [LinkWith] attribute. MonoDevelop auto-detects the
	// architectures that the native library supports and fills in that information for you,
	// however, it cannot auto-detect any Frameworks or other system libraries that the
	// native library may depend on, so you'll need to fill in that information yourself.
	//
	// Once you've done that, you're ready to move on to binding the API...
	//
	//
	// Here is where you'd define your API definition for the native Objective-C library.
	//
	// For example, to bind the following Objective-C class:
	//
	//     @interface Widget : NSObject {
	//     }
	//
	// The C# binding would look like this:
	//
	//     [BaseType (typeof (NSObject))]
	//     interface Widget {
	//     }
	//
	// To bind Objective-C properties, such as:
	//
	//     @property (nonatomic, readwrite, assign) CGPoint center;
	//
	// You would add a property definition in the C# interface like so:
	//
	//     [Export ("center")]
	//     PointF Center { get; set; }
	//
	// To bind an Objective-C method, such as:
	//
	//     -(void) doSomething:(NSObject *)object atIndex:(NSInteger)index;
	//
	// You would add a method definition to the C# interface like so:
	//
	//     [Export ("doSomething:atIndex:")]
	//     void DoSomething (NSObject object, int index);
	//
	// Objective-C "constructors" such as:
	//
	//     -(id)initWithElmo:(ElmoMuppet *)elmo;
	//
	// Can be bound as:
	//
	//     [Export ("initWithElmo:")]
	//     IntPtr Constructor (ElmoMuppet elmo);
	//
	// For more information, see http://docs.xamarin.com/ios/advanced_topics/binding_objective-c_types
	//

	
	[BaseType (typeof (UIView))]
	interface iCarousel {
  [Export ("initWithFrame:")]
  IntPtr Constructor (RectangleF frame);
  [Export ("dataSource")]
//		IBOutlet Id<iCarouselDelegate> { get; set;  }
        iCarouselDataSource DataSource { get; set; }

		[Wrap ("WeakDelegate")]
		iCarouselDelegate Delegate { get; set; }

  [Export ("delegate", ArgumentSemantic.Assign), NullAllowed]
        NSObject WeakDelegate { get; set; }
		[Export ("type")]
		iCarouselType Type { get; set;  }

		[Export ("perspective")]
		float Perspective { get; set;  }

		[Export ("decelerationRate")]
		float DecelerationRate { get; set;  }

		[Export ("scrollSpeed")]
		float ScrollSpeed { get; set;  }

		[Export ("bounceDistance")]
		float BounceDistance { get; set;  }

		[Export ("scrollEnabled")]
		bool ScrollEnabled { get; set;  }

		[Export ("wrapEnabled")]
		bool WrapEnabled { get;  }

		[Export ("bounces")]
		bool Bounces { get; set;  }

		[Export ("scrollOffset")]
  float ScrollOffset{ get; }

		[Export ("offsetMultiplier")]
		float OffsetMultiplier { get;  }

		[Export ("contentOffset")]
  SizeF ContentOffset{ get; }

		[Export ("viewpointOffset")]
		SizeF ViewpointOffset { get; set;  }

		[Export ("numberOfItems")]
		int NumberOfItems { get;  }

		[Export ("numberOfPlaceholders")]
		int NumberOfPlaceholders { get;  }

		[Export ("currentItemIndex")]
  int CurrentItemIndex{ get; }

		[Export ("currentItemView")]
		UIView CurrentItemView { get;  }
//  @property (nonatomic, strong, readonly) UIView *currentItemView;            //!
  // [Export ("currentItemView")]
//  int CurrentItemView{ get; }
		[Export ("indexesForVisibleItems")]
		NSArray IndexesForVisibleItems { get;  }

		[Export ("numberOfVisibleItems")]
		int NumberOfVisibleItems { get;  }

		[Export ("visibleItemViews")]
  UIView[] VisibleItemViews{ get; }

		[Export ("itemWidth")]
		float ItemWidth { get;  }

		[Export ("contentView")]
		UIView ContentView { get;  }

		[Export ("toggle")]
		float Toggle { get;  }

		[Export ("stopAtItemBoundary")]
		bool StopAtItemBoundary { get; set;  }

		[Export ("scrollToItemBoundary")]
		bool ScrollToItemBoundary { get; set;  }

		[Export ("useDisplayLink")]
		bool UseDisplayLink { get; set;  }

  [Export ("vertical")]
  bool Vertical{ [Bind ("isVertical")]get; set; }
		[Export ("ignorePerpendicularSwipes")]
		bool IgnorePerpendicularSwipes { get; set;  }

		[Export ("centerItemWhenSelected")]
		bool CenterItemWhenSelected { get; set;  }

  [Export ("scrollByNumberOfItems:itemCount:duration:")]
  void scrollByNumberOfItems( int itemCount, double duration );
//		[Export ("dataSource", ArgumentSemantic.Assign)][NullAllowed]
//		NSObject WeakDelegate2 { get; set;  }
//
//		[Wrap ("WeakDelegate2")]
//		iCarouselDataSource DataSource { get; set; }

  [Export ("scrollToItemAtIndex:index:duration:")]
  void scrollToItemAtIndex( int index, double duration );

  [Export ("scrollToItemAtIndex:index:animated:")]
  void scrollToItemAtIndex( int index, bool animated );

  [Export ("removeItemAtIndex:index:animated:")]
  void removeItemAtIndex( int index, bool animated );

  [Export ("insertItemAtIndex:index:animated:")]
  void insertItemAtIndex( int index, bool animated );

  [Export ("reloadItemAtIndex:index:animated:")]
  void reloadItemAtIndex( int index, bool animated );

		[Export ("itemViewAtIndex:")]
  UIView itemViewAtIndex( int index );

		[Export ("indexOfItemView:")]
  int indexOfItemView( UIView view );

		[Export ("indexOfItemViewOrSubview:")]
  int indexOfItemViewOrSubview( UIView view );

//  - (void)reloadData;




		[Export ("reloadData")]
  void reloadData();

	}

	[BaseType (typeof (NSObject))]
	[Model]
 interface iCarouselDataSource
 {
		[Export ("numberOfItemsInCarousel:")]
  int numberOfItemsInCarousel( iCarousel carousel );

//  - (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view;
		[Export ("carousel:viewForItemAtIndex:reusingView:")]
  UIView viewForItemAtIndex( iCarousel carousel, int index, UIView view );

//  @optional
  [Export ("numberOfVisibleItemsInCarousel:")]
  int numberOfVisibleItemsInCarousel( iCarousel carousel );

//  //deprecated, use carousel:viewForItemAtIndex:reusingView: and carousel:placeholderViewAtIndex:reusingView: instead

	}

	[BaseType (typeof (NSObject))]
	[Model]
	interface iCarouselDelegate {
		[Export ("carouselWillBeginScrollingAnimation:")]
  void WillBeginScrollingAnimation( iCarousel carousel );

		[Export ("carouselDidEndScrollingAnimation:")]
  void DidEndScrollingAnimation( iCarousel carousel );

		[Export ("carouselDidScroll:")]
  void DidScroll( iCarousel carousel );

  [Export ("carouselCurrentItemIndexUpdated:")]
  void CurrentItemIndexUpdated( iCarousel carousel );

		[Export ("carouselWillBeginDragging:")]
  void WillBeginDragging( iCarousel carousel );

  [Export ("carouselDidEndDragging:")]
  void DidEndDragging( iCarousel carousel );

		[Export ("carouselWillBeginDecelerating:")]
  void WillBeginDecelerating( iCarousel carousel );

		[Export ("carouselDidEndDecelerating:")]
  void DidEndDecelerating( iCarousel carousel );

//  - (CGFloat)carouselItemWidth:(iCarousel *)carousel;


		[Export ("carouselItemWidth:")]
  float ItemWidth( iCarousel carousel );

  [Export ("carouselOffsetMultiplier:")]
  float OffsetMultiplier( iCarousel carousel );
  [Export ("carouselShouldWrap:")]
  bool ShouldWrap( iCarousel carousel );
  [Export ("carousel:itemAlphaForOffset:")]
  float itemAlphaForOffset( iCarousel carousel, float offset );
		[Export ("carousel:itemTransformForOffset:baseTransform:")]
  CATransform3D carousel( iCarousel carousel, float offset, CATransform3D transform );

  [Export ("carousel:valueForTransformOption:withDefault:")]
  float valueForTransformOption( iCarousel carousel, iCarouselTranformOption option, float withDefault );
  [Export ("carousel:shouldSelectItemAtIndex:")]
  bool shouldSelectItemAtIndex( iCarousel carousel, int index );
  [Export ("carousel:didSelectItemAtIndex:")]
  void didSelectItemAtIndex( iCarousel carousel, int index );

	}

//	[BaseType (typeof (NSObject))]
//	[Model]
//	interface iCarouselDeprecated {
//		[Export ("numberOfVisibleItemsInCarousel:")]
//		uint NumberOfVisibleItemsInCarousel (iCarousel carousel);
//
//		[Export ("carouselCurrentItemIndexUpdated:__attribute__((deprecated))")]
//		void CarouselCurrentItemIndexUpdated__attribute__((deprecated)) (iCarousel carousel, (deprecated ));
//
//		[Export ("carouselShouldWrap:__attribute__((deprecated))")]
//		bool CarouselShouldWrap__attribute__((deprecated)) (iCarousel carousel, (deprecated ));
//
//		[Export ("carouselOffsetMultiplier:__attribute__((deprecated))")]
//		float CarouselOffsetMultiplier__attribute__((deprecated)) (iCarousel carousel, (deprecated ));
//
//		[Export ("carousel:itemAlphaForOffset:__attribute__((deprecated))")]
//		float CarouselitemAlphaForOffset__attribute__((deprecated)) (iCarousel carousel, float offset, (deprecated ));
//
//		[Export ("carousel:valueForTransformOption:withDefault:__attribute__((deprecated))")]
//		float CarouselvalueForTransformOptionwithDefault__attribute__((deprecated)) (iCarousel carousel, iCarouselOption option, float value, (deprecated ));
//
//	}

}

