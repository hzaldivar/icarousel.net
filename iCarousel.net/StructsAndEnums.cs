using System;

namespace iCarousel.net
{
	public enum iCarouselType {
		iCarouselTypeLinear = 0,
	    iCarouselTypeRotary,
	    iCarouselTypeInvertedRotary,
	    iCarouselTypeCylinder,
	    iCarouselTypeInvertedCylinder,
	    iCarouselTypeWheel,
	    iCarouselTypeInvertedWheel,
	    iCarouselTypeCoverFlow,
	    iCarouselTypeCoverFlow2,
	    iCarouselTypeTimeMachine,
	    iCarouselTypeInvertedTimeMachine,
	    iCarouselTypeCustom
	}

	public enum iCarouselOption {
		iCarouselOptionWrap = 0,
	    iCarouselOptionShowBackfaces,
	    iCarouselOptionOffsetMultiplier,
	    iCarouselOptionVisibleItems,
	    iCarouselOptionCount,
	    iCarouselOptionArc,
		iCarouselOptionAngle,
	    iCarouselOptionRadius,
	    iCarouselOptionTilt,
	    iCarouselOptionSpacing,
	    iCarouselOptionFadeMin,
	    iCarouselOptionFadeMax,
	    iCarouselOptionFadeRange
	}

	public enum iCarouselTranformOption
	{
	    iCarouselTranformOptionCount = 0,
	    iCarouselTranformOptionArc,
	 	iCarouselTranformOptionAngle,
	    iCarouselTranformOptionRadius,
	    iCarouselTranformOptionTilt,
	    iCarouselTranformOptionSpacing
	}
}

